terraform {
  source = "git::https://gitlab.com/aarnvns/terraform-aws-s3-static-site.git//?ref=v1"
}

include {
  path = find_in_parent_folders()
}

inputs = {
  route53_hosted_zone_name          = "unmonetized.com"
  route53_static_site_name          = "www"
  do_create_redirect_site           = true
}