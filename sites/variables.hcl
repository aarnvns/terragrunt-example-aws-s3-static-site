# defines global variables necessary for each cluster under the "clusters" directory
locals {
  # general settings
  aws_region = "us-east-1"

  # terraform settings
  terraform_aws_region        = "us-east-2"
  terraform_lock_table_name   = "terraform-lock-table"
  terraform_state_bucket_name = "${get_aws_account_id()}-terraform-state"
}
