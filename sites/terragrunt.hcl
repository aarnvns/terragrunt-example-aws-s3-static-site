locals {
  cluster_vars                = read_terragrunt_config(find_in_parent_folders("variables.hcl"))
  aws_region                  = local.cluster_vars.locals.aws_region
  terraform_aws_region        = local.cluster_vars.locals.terraform_aws_region
  terraform_state_bucket_name = local.cluster_vars.locals.terraform_state_bucket_name
  terraform_lock_table_name   = local.cluster_vars.locals.terraform_lock_table_name
}

generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
provider "aws" {
  region = "${local.aws_region}"
}
EOF
}

remote_state {
  backend = "s3"
  config = {
    encrypt        = true
    bucket         = local.terraform_state_bucket_name
    key            = "${path_relative_to_include()}/terraform.tfstate"
    region         = local.terraform_aws_region
    encrypt        = true
    dynamodb_table = local.terraform_lock_table_name
  }
  generate = {
    path      = "backend.tf"
    if_exists = "overwrite_terragrunt"
  }
}
