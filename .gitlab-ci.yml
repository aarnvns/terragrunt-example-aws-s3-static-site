stages:
  - validate
  - deploy
  - s3

variables:
  VERSION_TERRAFORM: "0.12.24"
  VERSION_TERRAGRUNT: "v0.23.10"
  VERSION_TFLINT: "v0.15.5"

.terraform:
  image: ubuntu:20.04
  stage: deploy
  before_script:
    - |-
      apt-get update
      DEBIAN_FRONTEND=noninteractive apt-get install -y awscli git python wget unzip

      # install terraform
      wget -q "https://releases.hashicorp.com/terraform/${VERSION_TERRAFORM}/terraform_${VERSION_TERRAFORM}_linux_amd64.zip" -O terraform.zip && unzip terraform.zip -d /usr/local/bin && rm terraform.zip
      chmod 755 /usr/local/bin/terraform

      # install terragrunt
      wget -q "https://github.com/gruntwork-io/terragrunt/releases/download/${VERSION_TERRAGRUNT}/terragrunt_linux_amd64" -O /usr/local/bin/terragrunt
      chmod 755 /usr/local/bin/terragrunt

      # install tflint
      wget -q "https://github.com/terraform-linters/tflint/releases/download/${VERSION_TFLINT}/tflint_linux_amd64.zip" -O tflint.zip && unzip tflint.zip -d /usr/local/bin && rm tflint.zip
      chmod 755 /usr/local/bin/tflint

      # install pre-commit
      wget -q -O - "https://pre-commit.com/install-local.py" | python -
      ln -s ~/.pre-commit-venv/bin/pre-commit /usr/local/bin/pre-commit


Terraform Lint:
  extends: .terraform
  stage: validate
  script:
    - |-
      # must set a valid region during validation
      # see: https://github.com/hashicorp/terraform/issues/21408
      export AWS_REGION=us-east-2

      pre-commit run --all-files
  only:
    refs:
      - master
      - merge_requests

Terraform Plan:
  extends: .terraform
  stage: deploy
  script:
    - |-
      for d in $(find sites -type d -maxdepth 1 -mindepth 1); do
        echo $d
        pushd $d
        terragrunt plan-all --terragrunt-non-interactive
        popd
      done
  only:
    refs:
      - merge_requests

Terraform Apply:
  extends: .terraform
  stage: deploy
  script:
    - |-
      for d in $(find sites -type d -maxdepth 1 -mindepth 1); do
        echo $d
        pushd $d
        terragrunt apply-all -auto-approve --terragrunt-non-interactive
        popd
      done
  artifacts:
    paths:
      - cluster/artifacts/*
  only:
    refs:
      - master

Publish to S3:
  extends: .terraform
  stage: s3
  script:
    - |-
      for d in $(find sites -type d -maxdepth 1 -mindepth 1); do
        pushd $d
        pwd=$(pwd)
        dirname=$(basename $pwd)
        cd site-src
        echo upload to bucket $dirname
        aws s3 sync . s3://$dirname
        popd
      done

  only:
    refs:
      - master
