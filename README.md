# Terragrunt Example for the AWS Static Site Terraform module 

This is an example of wrappering the module [terraform-aws-s3-static-site](https://gitlab.com/aarnvns/terraform-aws-s3-static-site).

It's easy to use any of the three configuration methods described in *terraform-aws-s3-static-site* here.  The essential
utility provided by this example is the addition of a GitLab pipeline and a longer documentary piece on how
the module works.

## License

Open Source under [LICENSE.md](LICENSE.md)
